﻿using System.Collections.Generic;
using System.Linq;
using hmrc_checkout_test.Services;

namespace hmrc_checkout_test.Domain
{
    public class BuyOneGetOneFreeAppleOffer : IProductOffer
    {
        public decimal GetDiscount(List<IProduct> basket)
        {
            var count = basket.OfType<Apple>().Count();

            if (count > 1)
            {                
                var unitPrice = basket.OfType<Apple>().First().GetPrice();
                var divisor = count / 2;
                return divisor * unitPrice;
            }

            return 0;
        }
    }
}
