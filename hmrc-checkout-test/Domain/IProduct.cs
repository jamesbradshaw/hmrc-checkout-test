﻿namespace hmrc_checkout_test.Domain
{
    public interface IProduct
    {
        decimal GetPrice();
    }
}