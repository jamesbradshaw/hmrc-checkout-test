using hmrc_checkout_test.Domain;

namespace hmrc_checkout_test.Services
{
    public abstract class ProductBase : IProduct
    {
        private readonly decimal _price;

        protected ProductBase(decimal price)
        {
            _price = price;
        }

        public decimal GetPrice()
        {
            return _price;
        }
    }
}