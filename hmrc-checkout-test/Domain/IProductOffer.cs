﻿using System.Collections.Generic;
using hmrc_checkout_test.Services;

namespace hmrc_checkout_test.Domain
{
    public interface IProductOffer
    {
        decimal GetDiscount(List<IProduct> basket);
    }
}