﻿using System.Collections.Generic;
using System.Linq;

namespace hmrc_checkout_test.Domain
{
    public class Checkout
    {
        public Checkout()
        {
            Products = Enumerable.Empty<IProduct>().ToList();
            ProductOffers = Enumerable.Empty<IProductOffer>().ToList();
        }

        public List<IProduct> Products { get; set; }
        public List<IProductOffer> ProductOffers { get; set; }

        public decimal GetTotal()
        {
            var subTotal = GetSubTotal();
            var discount = GetDiscount();
            return subTotal + discount;
        }

        private decimal GetDiscount()
        {
            var discounts = ProductOffers.Select(po => po.GetDiscount(Products));
            return (discounts.Sum() * -1);
        }

        private decimal GetSubTotal()
        {
            var subTotal = Products.Sum(p => p.GetPrice());
            return subTotal;
        }
    }
}
