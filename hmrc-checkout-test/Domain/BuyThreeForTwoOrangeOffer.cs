﻿using System;
using System.Collections.Generic;
using System.Linq;
using hmrc_checkout_test.Services;

namespace hmrc_checkout_test.Domain
{
    public class BuyThreeForTwoOrangeOffer : IProductOffer
    {
        public decimal GetDiscount(List<IProduct> basket)
        {
            var count = basket.OfType<Orange>().Count();
            if (count >= 3)
            {
                var unitPrice = basket.OfType<Orange>().First().GetPrice();
                var multiplier = count / 3;
                return (multiplier * unitPrice);
            }

            return 0;
        }
    }
}