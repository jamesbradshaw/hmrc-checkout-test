﻿using System;
using System.Collections.Generic;
using System.Linq;
using hmrc_checkout_test.Domain;
using hmrc_checkout_test.Repositories;

namespace hmrc_checkout_test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("====================================");
            Console.WriteLine("- HRMC Checkout exercise           -");
            Console.WriteLine("- Completed by James Bradshaw      -");
            Console.WriteLine("- Grey Metis Ltd                   -");
            Console.WriteLine("- Contact: james@greymetis.co.uk   -");
            Console.WriteLine("------------------------------------");
            Console.WriteLine("- Enter codes separated by a ,     -");
            Console.WriteLine("- Supported codes: apple, orange   -");
            Console.WriteLine("- Type exit to close               -");
            Console.WriteLine("====================================");

            var isRunning = true;
            var productRepository = new ProductRepository();
            

            while (isRunning)
            {
                Console.WriteLine("");
                Console.Write("Enter codes or exit: ");
                var input = Console.ReadLine();

                if (input.ToLower() == "exit")
                {
                    isRunning = false;
                }
                else
                {
                    var codes = input.Split(",").ToList();
                    var basket = new List<IProduct>();
                    codes.ForEach(c => basket.Add(productRepository.GetProductByCode(c.TrimStart().TrimEnd())));

                    var checkout = new Checkout
                    {
                        Products = basket,
                        ProductOffers = new List<IProductOffer>() { new BuyOneGetOneFreeAppleOffer(), new BuyThreeForTwoOrangeOffer() }
                    };

                    var total = checkout.GetTotal();

                    Console.WriteLine($"{basket.Count} item(s) cost {total:C}");
                }
            }           
        }
    }
}
