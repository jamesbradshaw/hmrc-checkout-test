﻿using System;
using hmrc_checkout_test.Domain;
using hmrc_checkout_test.Services;

namespace hmrc_checkout_test.Repositories
{
    public class ProductRepository : IProductRepository
    {
        public IProduct GetProductByCode(string code)
        {
            if (code.ToLower() == "apple")
            {
                return new Apple();
            }
            if (code.ToLower() == "orange")
            {
                return new Orange();
            }
            
            throw new Exception("Unexpected item in bagging area.");
        }
    }
}