﻿using hmrc_checkout_test.Domain;

namespace hmrc_checkout_test.Repositories
{
    public interface IProductRepository
    {
        /// <summary>
        /// Gets a product located by product code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        IProduct GetProductByCode(string code);
    }
}
