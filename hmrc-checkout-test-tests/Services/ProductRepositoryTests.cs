using System;
using hmrc_checkout_test.Repositories;
using hmrc_checkout_test.Services;
using Xunit;

namespace hmrc_checkout_test_tests
{
    public class ProductRepositoryTests
    {
        [Theory]
        [InlineData("Apple")]
        [InlineData("apple")]
        [InlineData("APPLE")]
        public void GetProductByCode_InputApple_ReturnsProductTypeApple(string code)
        {
            // Arrange
            var sut = new ProductRepository();

            // Act
            var result = sut.GetProductByCode(code);

            Assert.IsType<Apple>(result);
        }

        [Theory]
        [InlineData("Orange")]
        [InlineData("orange")]
        [InlineData("ORANGE")]
        public void GetProductByCode_InputOrange_ReturnsProductTypeOrange(string code)
        {
            // Arrange
            var sut = new ProductRepository();

            // Act
            var result = sut.GetProductByCode(code);

            // Assert
            Assert.IsType<Orange>(result);
        }

        [Fact]
        public void GetProductByCode_InputInvalidCode_ThrowsException()
        {
            // Arrange
            var sut = new ProductRepository();

            // Act
            var exception = Assert.Throws<Exception>(() => sut.GetProductByCode("coconut"));

            // Assert
            Assert.Equal("Unexpected item in bagging area.", exception.Message);
        }
    }
}
