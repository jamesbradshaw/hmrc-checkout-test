﻿using System.Collections.Generic;
using hmrc_checkout_test.Domain;
using hmrc_checkout_test.Services;
using Xunit;

namespace hmrc_checkout_test_tests.Domain
{
    public class BuyOneGetOneFreeAppleOfferTests
    {
        [Fact]
        public void GetDiscount_NoApplesInBasket_returnZero()
        {
            // Arrange
            var sut = new BuyOneGetOneFreeAppleOffer();
            
            // Act
            var result = sut.GetDiscount(new List<IProduct>() { new Orange(), new Orange() });

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetDiscount_OneApplesInBasket_ReturnZero()
        {
            // Arrange
            var sut = new BuyOneGetOneFreeAppleOffer();

            // Act
            var result = sut.GetDiscount(new List<IProduct>() { new Apple() });

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetDiscount_TwoApplesInBasket_ReturnDiscountForOneApple()
        {
            // Arrange
            var sut = new BuyOneGetOneFreeAppleOffer();
            var apple = new Apple();
            var expected = apple.GetPrice();

            // Act
            var result = sut.GetDiscount(new List<IProduct>() { new Apple(), apple });

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetDiscount_ThreeApplesInBasket_ReturnDiscountForOneApple()
        {
            // Arrange
            var sut = new BuyOneGetOneFreeAppleOffer();
            var apple = new Apple();
            var expected = apple.GetPrice();

            // Act
            var result = sut.GetDiscount(new List<IProduct>() { new Apple(), new Apple(), apple });

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetDiscount_FourApplesInBasket_ReturnDiscountForTwoApples()
        {
            // Arrange
            var sut = new BuyOneGetOneFreeAppleOffer();
            var apple = new Apple();
            var expected = (apple.GetPrice()*2);

            // Act
            var result = sut.GetDiscount(new List<IProduct>() { new Apple(), new Apple(), new Apple(), apple });

            // Assert
            Assert.Equal(expected, result);
        }
    }
}
