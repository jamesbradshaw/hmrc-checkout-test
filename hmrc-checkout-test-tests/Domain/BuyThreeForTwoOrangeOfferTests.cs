﻿using System;
using System.Collections.Generic;
using hmrc_checkout_test.Domain;
using hmrc_checkout_test.Services;
using Xunit;

namespace hmrc_checkout_test_tests.Domain
{
    public class BuyThreeForTwoOrangeOfferTests
    {
        [Theory]
        [MemberData(nameof(InvalidForOrangeDiscount))]
        public void GetDiscount_BasketsThatDontQualifyForDiscount_ReturnZero(List<IProduct> basket)
        {
            // Arrange
            var sut = new BuyThreeForTwoOrangeOffer();

            // Act
            var result = sut.GetDiscount(basket);

            // Assert
            Assert.Equal(0, result);
        }

        [Theory]
        [MemberData(nameof(ValidForOrangeDiscount))]
        public void GetDiscount_BasketsThatQualifyForDiscount_ReturnDiscountAmount(List<IProduct> basket, decimal expected)
        {
            // Arrange
            var sut = new BuyThreeForTwoOrangeOffer();

            // Act
            var result = sut.GetDiscount(basket);

            // Assert
            Assert.Equal(expected, result);
        }

        public static IEnumerable<object[]> ValidForOrangeDiscount()
        {
            var unitPrice = new Orange().GetPrice();
            return new List<object[]>
            {
                new object[] {new List<IProduct>() { new Orange(), new Orange(), new Orange() }, unitPrice }, // qualifies once
                new object[] {new List<IProduct>() { new Orange(), new Orange(), new Orange(), new Orange() }, unitPrice }, // qualifies once
                new object[] {new List<IProduct>() { new Orange(), new Orange(), new Orange(), new Orange(), new Orange() }, unitPrice }, // qualifies once
                new object[] {new List<IProduct>() { new Orange(), new Orange(), new Orange(), new Orange(), new Orange(), new Orange() }, (unitPrice * 2) },  // qualifies twice
            };
        }

        public static IEnumerable<object[]> InvalidForOrangeDiscount =>
            new List<object[]>
            {
                new object[] {new List<IProduct>() {new Apple()}},
                new object[] {new List<IProduct>() {new Orange()}},
                new object[] {new List<IProduct>() {new Orange(), new Orange()}},
            };
    }
}