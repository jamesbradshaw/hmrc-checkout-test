﻿using System.Collections.Generic;
using hmrc_checkout_test.Domain;
using hmrc_checkout_test.Services;
using Xunit;

namespace hmrc_checkout_test_tests.Domain
{
    public class CheckoutTests
    {
        [Fact]
        public void Total_NoItems_ReturnsZero()
        {
            // Arrange
            var sut = new Checkout();

            // Act
            var result = sut.GetTotal();

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void Total_HasTwoDifferentTimes_ReturnsSumOfBasket()
        {
            // Arrange
            var apple = new Apple();
            var orange = new Orange();
            var expected = (apple.GetPrice() + orange.GetPrice());

            var sut = new Checkout {Products = new List<IProduct>() {apple, orange}};

            // Act
            var result = sut.GetTotal();

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Total_DiscountAppliesToBasket_ReturnsDiscountedPrice()
        {
            // Arrange
            var apple = new Apple();
            var orange = new Orange();
            var discount = 0.11m;
            var expected = (apple.GetPrice() + orange.GetPrice()) - discount;

            var sut = new Checkout
            {
                Products = new List<IProduct>() {apple, orange},
                ProductOffers = new List<IProductOffer>() { new StubOffer(discount) }
            };

            // Act
            var result = sut.GetTotal();

            // Assert
            Assert.Equal(expected, result);
        }

        internal class StubOffer : IProductOffer
        {
            private readonly decimal _returnValue;

            public StubOffer(decimal returnValue)
            {
                _returnValue = returnValue;
            }

            public decimal GetDiscount(List<IProduct> basket)
            {
                return _returnValue;
            }
        }
    }
}
