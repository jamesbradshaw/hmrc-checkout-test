﻿using hmrc_checkout_test.Services;
using Xunit;

namespace hmrc_checkout_test_tests.Domain
{
    public class ProductTests
    {
        [Fact]
        public void GetPrice_ApplePrice_Is60pence()
        {
            // Arrange
            var expected = 0.60m;
            var sut = new Apple();

            // Act
            var result = sut.GetPrice();

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetPrice_OrangePrice_Is25pence()
        {
            // Arrange
            var expected = 0.25m;
            var sut = new Orange();

            // Act
            var result = sut.GetPrice();

            // Assert
            Assert.Equal(expected, result);
        }
    }
}
